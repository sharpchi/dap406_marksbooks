<?php
require_once('../config.php');
$query = '';
$results = [];
if (isset($_POST['submit']) || isset($_GET['q'])) {
    // validate query
    $query = isset($_POST['search']) ? $_POST['search'] : $_GET['q'];
    $currentpage = isset($_GET['p']) ? $_GET['p'] : 1;
    $itemsperpage = 10;
    $results = $DB->searchBooks($query, $currentpage, $itemsperpage);
}

$pagetitle = 'Search books';
include($CFG->dirroot . '/inc/header.php');
?>

<form method="post" action="search.php">
    <div class="input-group">
        <input name="search" class="form-control" value="<?php echo $query; ?>" type="text" placeholder="Search books" />
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default" name="submit">Search</button>
        </span>
    </div>
</form>

<?php

if ($results) {
    ?>
    <h2>Results</h2>
    <ul>
    <?php
    foreach ($results->books as $result) {
        echo '<li><a href="view.php?id=' . $result->id   . '">' . $result->title . '</a></li>';
    }
    ?>
    </ul>
    <?php
    paginator(['q' => $query], $currentpage, $results->totalpages);
}

include($CFG->dirroot . '/inc/footer.php');
