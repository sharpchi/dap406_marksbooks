<?php

session_start();
$isLoggedin = false;

if (isset($_SESSION['loggedin'])) {
    $isLoggedin = true;
    $username = $_SESSION['user']['name'];
}

function login($username, $password) {
    global $DB, $CFG;
    $errors = [];
    $hashedpassword = hash('sha256', $CFG->salt . $password);
    $result = $DB->login($username, $hashedpassword);
    if ($result) {
        $loggedin = true;
        $_SESSION['loggedin'] = $loggedin;
        $_SESSION['user'] = $result;
        $user = $result;
    } else {
        $errors['failed'] = 'You have not entered correct credentials.';
    }
    return $errors;
}

function logout() {
    global $loggedin;
    session_unset();
    session_destroy();
    $isLoggedin = false;
}

function requirelogin() {
    global $isLoggedin, $CFG;
    if (!$isLoggedin) {
        header('HTTP/1.1 403 Forbidden');
        header('Location: ' . $CFG->www . '/errors/403.php');
        die();
    }
}
