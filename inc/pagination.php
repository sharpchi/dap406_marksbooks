<?php

function paginator($params = [], $currentpage = 1, $totalpages = 1) {
    global $CFG;
    if ($totalpages == 1) {
        return;
    }
    echo '<ul class="pagination">';
    for ($x=1; $x <= $totalpages; $x++) {
        $active = ($currentpage == $x) ? ' active' : '';
        $params['p'] = $x;
        $qstring = [];
        foreach($params as $k => $v) {
            $qstring[] = $k . '=' . $v;
        }
        echo '<li class="page-item' . $active . '"><a href="?' . join('&', $qstring) . '" class="page-link">' . $x . '</a></li>';
    }
    echo '</ul>';
}
