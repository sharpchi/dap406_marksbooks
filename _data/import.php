<?php

require_once('../config.php');

$row = 1;
if (($handle = fopen("top_100_books.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        $r = [];
        $r['author'] = explode(',', $data[4]);
        $r['publisher'] = $data[5];
        $r['title'] = $data[3];
        $r['isbn'] = $data[2];
        $r['published'] = explode(' ', $data[12]);
        if (count($r['author']) !== 2) {
            // skip this book. No authors.
            continue;
        }
        $r['author_firstname'] = trim($r['author'][1]);
        $r['author_lastname'] = trim($r['author'][0]);
        $r['yearpublished'] = trim($r['published'][2]);

        $author = $DB->getRecord('author', ['firstname' => $r['author_firstname'], 'lastname' => $r['author_lastname']]);
        if (!$author) {
            $a = new stdClass();
            $a->firstname = $r['author_firstname'];
            $a->lastname = $r['author_lastname'];
            $a->email = '';
            $a->www = '';
            $a->twitter = '';
            $author = $DB->insertAuthor($a);
        } else {
            $author = $author->id;
        }

        $publisher = $DB->getRecord('publisher', ['name' => $r['publisher']]);
        if (!$publisher) {
            $p = new stdClass();
            $p->name = $r['publisher'];
            $p->address1 = '';
            $p->address2 = '';
            $p->town = '';
            $p->county = '';
            $p->country = '';
            $p->postcode = '';
            $p->phone = '';
            $p->www = '';
            $p->twitter = '';
            $publisher = $DB->insertPublisher($p);
        } else {
            $publisher = $publisher->id;
        }

        $title = $DB->getRecord('book', ['title' => $r['title']]);
        if (!$title) {
            $b = new stdClass();
            $b->title = $r['title'];
            $b->yearpublished = $r['yearpublished'];
            $b->isbn = $r['isbn'];
            $b->publisherid = $publisher;
            $b->authors = [$author];
            $DB->insertBook($b);
        }

        print_r($r);

    }
    fclose($handle);
}
