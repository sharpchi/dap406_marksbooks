<?php
$menu = [
    'home' => [
        'url' => '/index.php',
        'label' => 'Home'
    ],
    'book' => [
        'url' => '/book/',
        'label' => 'Books',
        'children' => [
            'search' => [
                'url' => '/book/search.php',
                'label' => 'Books'
            ],
            'edit' => [
                'url' => '/book/edit.php',
                'label' => 'New book',
                'requirelogin' => 1
            ]
        ]
    ],
    'publisher' => [
        'url' => '/publisher/',
        'label' => 'Publishers',
        'children' => [
            'search' => [
                'url' => '/publisher/search.php',
                'label' => 'Publishers'
            ],
            'edit' => [
                'url' => '/publisher/edit.php',
                'label' => 'New publisher',
                'requirelogin' => 1
            ]
        ]
    ],
    'author' => [
        'url' => '/author/',
        'label' => 'Authors',
        'children' => [
            'search' => [
                'url' => '/author/search.php',
                'label' => 'Authors'
            ],
            'edit' => [
                'url' => '/author/edit.php',
                'label' => 'New author',
                'requirelogin' => 1
            ]
        ]
    ],
    // 'genre' => [
    //     'url' => '/genre/',
    //     'label' => 'Genres'
    // ]
];

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?php echo $CFG->www; ?>">Mark's Books</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php
        //echo $page;
        foreach ($menu as $key => $item) {
            $dropdown = isset($item['children']) ? ' dropdown' : '';
            $active = ($item['url'] == $page) ? ' active' : '';
            echo '<li class="nav-item' . $active . $dropdown . '">';

            if ($dropdown) {
                echo '<a class="nav-link dropdown-toggle" href="' . $CFG->www . $item['url'] . '" data-toggle="dropdown">' .
                    $item['label'] . '</a>';

                $childmenu = '';
                foreach ($item['children'] as $child) {
                    if ((isset($child['requirelogin']) && $isLoggedin) || !isset($child['requirelogin'])) {
                        $childmenu .= '<a class="dropdown-item" href="' . $CFG->www . $child['url'] . '">' . $child['label'] . '</a>';
                    }

                }
                if ($childmenu !== '') {
                    echo '<div class="dropdown-menu">' . $childmenu . '</div>';
                }

            } else {
                if (isset($item['requirelogin']) && $isLoggedin) {
                    echo '<a class="nav-link" href="' . $CFG->www . $item['url'] . '">' .
                        $item['label'] . '</a>';
                }

            }
            echo '</li>';
        }
        ?>
    </ul>

    <form class="form-inline my-2 my-lg-0" method="POST" action="<?php echo $CFG->www; ?>/book/search.php">
      <input class="form-control mr-sm-2" type="text" placeholder="Search books" aria-label="Search books">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <?php
    if ($isLoggedin) {
        echo '<a href="' . $CFG->www . '/login.php?logout=1" class="btn btn-danger loginbutton">Logout</a>';
    } else {
        echo '<a href="' . $CFG->www . '/login.php" class="btn btn-primary loginbutton">Login</a>';
    }
     ?>

  </div>
</nav>
