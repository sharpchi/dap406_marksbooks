<?php
include('config.php');
if (isset($_GET['logout'])) {
    logout();
    header('Location: ' . $CFG->www);
}

if (isset($_POST['login'])) {
    $errors = [];
    $username = isset($_POST['username']) ? trim($_POST['username']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';

    if (empty($username)) {
        $errors['username'] = 'You must enter a username.';
    }
    if (empty($password)) {
        $errors['password'] = 'You must enter a password.';
    }

    if (count($errors) === 0) {
        $result = login($username, $password);
        if (count($result) > 0) {
            $errors = $result;
        }
    }
}
$pagetitle = 'Login';
include($CFG->dirroot . '/inc/header.php');
?>

        <form action="" method="post">
            <?php
            if ($isLoggedin) {
                ?>
                <p>Hello, <?php echo $username; ?></p>
                <div>
                    <a href="<?php echo $CFG->www; ?>/login.php?logout=1" class="btn btn-danger">Logout</a>
                </div>
                <?php
            } else {
                if (isset($errors['failed'])) {
                    echo '<div class="alert error">' . $errors['failed'] . '</div>';
                }
                ?>
               <div class="form-group">
                   <?php
                       $class = isset($errors['username']) ? 'error' : '';
                    ?>
                   <label for="username">Username *:</label>
                   <input type="text" name="username" value="" autocomplete="off" class="form-control <?php echo $class; ?>" />
                   <?php if (isset($errors['username'])) {
                       echo '<span class="error">' . $errors['username'] . '</span>';
                   }
                   ?>
               </div>
               <div class="form-group">
                   <?php
                       $class = isset($errors['password']) ? 'error' : '';
                    ?>
                   <label for="password">Password *:</label>
                   <input type="password" name="password" value="" autocomplete="off" class="form-control <?php echo $class; ?>" />
                   <?php if (isset($errors['password'])) {
                       echo '<span class="error">' . $errors['password'] . '</span>';
                   }
                   ?>
               </div>
               <input type="submit" name="login" value="login" class="btn btn-primary" />
               <?php
            }
            ?>
        </form>
<?php
include($CFG->dirroot . '/inc/footer.php');
